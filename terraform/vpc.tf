module "vpc" {
  source         = "terraform-aws-modules/vpc/aws"
  version        = "2.38.0"
  name           = "test_ecs_provisioning"
  cidr           = "10.10.0.0/16"
  azs            = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  public_subnets = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
}

data "aws_vpc" "main" {
  id = module.vpc.vpc_id
}